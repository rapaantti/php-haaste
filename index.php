<?php
require_once __DIR__ . '/init.php';

use classes\Request;
use classes\tasks\interfaces\Executable;

$request = new Request();
$class = 'classes\\tasks\\' . ucfirst($request->getParam('path'));

if (class_exists($class)) {
    $task = new $class();

    if ($task instanceof Executable) {
        echo $task->exec();
    }
}
