<?php
/**
 * Autoload function that loads classes dynamically
 *
 * @param string $className
 */
function __autoload(string $className)
{
    $path = __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', '/', $className);
    require_once $path .  '.php';
}