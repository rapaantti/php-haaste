# PHP-haaste (PHP-challenge)

## Requirements:
* Apache 2
* PHP 7

## Installation:
```
#!bash
sudo a2enmod rewrite && sudo service apache2 restart
```

After that you have to change apache.conf directory's setting to **AllowOverride All**

## Other

Link to the PHP-challenge: [http://www.ohjelmointiputka.net/phph/](http://www.ohjelmointiputka.net/phph/)