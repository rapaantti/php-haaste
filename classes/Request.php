<?php
namespace classes;

/**
 * Simple request class to store $_REQUEST data
 *
 * @package classes
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Request
{
    /**
     * @var array
     */
    private $data;

    /**
     * Simple constructor for class
     */
    public function __construct()
    {
        $this->setData($_REQUEST);
    }

    /**
     * Sets data to request array
     *
     * @param mixed     $data      Data/Payload
     *
     * @return void
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * Returns method data using name.
     *
     * @param string    $name       Parameter name
     * @param mixed     $default    Default value if parameter does not exist.
     *
     * @return mixed
     */
    public function getParam(string $name, $default = null)
    {
        return $this->data[$name] ?? $default;
    }
}
