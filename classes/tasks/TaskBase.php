<?php
namespace classes\tasks;

use classes\Request;
use classes\tasks\interfaces\Executable;

/**
 * Base class for all tasks
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
abstract class TaskBase implements Executable
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * Constructor for class
     */
    public function __construct()
    {
        $this->request = new Request();
    }
}
