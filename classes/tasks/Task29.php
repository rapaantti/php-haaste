<?php
namespace classes\tasks;

/**
 * Implementation of Task 29
 *
 * Description:
 *
 * Kun sanaa kierretään vasemmalle, sen ensimmäinen kirjain siirtyy viimeiseksi. Kun sanaa kierretään oikealle,
 * sen viimeinen kirjain siirtyy ensimmäiseksi. Esimerkiksi jos sanaa AVARUUS kierretään kolmesti vasemmalle,
 * lopputulos on RUUSAVA.
 *
 * Tehtävänä on selvittää, saako sanan muutettua toiseksi kiertämällä sitä. Esimerkiksi sanan AVARUUS saa muutettua
 * sanaksi RUUSAVA ja samoin sanan RUUSAVA saa muutettua sanaksi AVARUUS. Kuitenkaan sanaa AVARUUS ei saa
 * muutettua sanaksi VAURAUS.
 *
 * Voit olettaa, että sanassa on korkeintaan sata kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=29
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task29 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word1 = $this->request->getParam('sana1', '');
        $word2 = $this->request->getParam('sana2', '');

        if ($word1 === $word2) {
            return 1;
        }

        $characters = str_split($word1);

        $count = count($characters);
        $i     = 0;

        while ($i < $count) {
            $word = $this->shiftWord($characters, ++$i);

            if ($word === $word2) {
                return 1;
            }
        }

        return 0;
    }

    /**
     * Method to shift array of character to left given amount.
     *
     * @param array    $characters
     * @param int      $amount
     *
     * @return string
     */
    private function shiftWord(array $characters, int $amount) : string
    {
        $output = [];
        $count  = count($characters);

        foreach ($characters as $key => $character) {
            $newKey = (($key - $amount) % $count);

            if ($newKey < 0) { // Sorry, php's modulo returns negative values.
                $newKey = $count + $newKey;
            }

            $output[$newKey] = $character;
        }

        ksort($output);

        return implode('', $output);
    }
}
