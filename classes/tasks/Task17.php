<?php
namespace classes\tasks;

/**
 * Implementation of Task 17
 *
 * Description:
 *
 * Lukujen 4, 1, 3 ja 5 joukossa ovat kaikki luvut väliltä 1–5 paitsi luku 2. Tehtävässä annetaan jossain järjestyksessä
 * kaikki luvut väliltä 1–n yhtä lukuun ottamatta, ja tarkoitus on selvittää, mikä luku puuttuu.
 * Voit olettaa, että n on korkeintaan 1000 ja jokainen luku annetaan vain kerran.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=17
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task17 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $numbersCount = (int)$this->request->getParam('n', 0);
        $numberString = $this->request->getParam('luvut', '');

        $numbers = array_map('intval', explode('|', $numberString));

        $allNumbers = array_keys(array_fill(0, $numbersCount + 1, 1));

        array_shift($allNumbers);

        return current(array_diff($allNumbers, $numbers));
    }
}
