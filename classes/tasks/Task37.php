<?php
namespace classes\tasks;

/**
 * Implementation of Task 37
 *
 * Description:
 *
 * Kalle laittaa ensimmäisenä päivänä hiekalle yhden neliön muotoisen kiven. Toisena päivänä hän laittaa kiven ympärille
 * uuden kerroksen kiviä. Kolmantena päivänä hän laittaa kivien ympärille jälleen uuden kerroksen kiviä. Sama toistuu
 * muinakin päivinä. Kivistä muodostuu seuraava kuvio:
 *
 * Yllä oleva kuvio vastaa tilannetta neljäntenä päivänä. Millainen kuvio on päivänä n? Voit olettaa, että n on
 * korkeintaan 15.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=37
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task37 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', '');
        $width  = ($number + ($number - 1));
        $char   = 64 + $number;
        $array  = [];
        $k      = 0;

        while ($k < $number) {
            for ($i = $k; $i < $width - $k; $i++) {
                for ($j = $k; $j < $width - $k; $j++) {
                    $array[$i][$j] = chr($char - $k);
                }
            }

            $k++;
        }

        return $this->output($array);
    }

    /**
     * Helper method to output array content.
     *
     * @param array $array
     *
     * @return string
     */
    private function output(array $array): string
    {
        $output = '';

        foreach ($array as $row) {
            $output .= implode('', $row) . '<br>';
        }

        return $output;
    }
}
