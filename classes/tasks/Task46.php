<?php
namespace classes\tasks;

/**
 * Implementation of Task 46
 *
 * Description:
 *
 * Kalle kirjoittaa ensin yhden A:n, sitten kaksi B:tä, sitten kolme A:ta, sitten neljä B:tä, jne., eli vuorotellen
 * kirjaimia A ja B ja samaa kirjainta aina yhden enemmän. Muodostuu siis seuraava äärettömän pitkä merkkijono:
 *
 * ABBAAABBBBAAAAABBBBBBAAAAAAABBBBBBBB...
 *
 * Kuudes Kallen kirjoittama kirjain on A. Mikä on n:s kirjain, jonka Kalle kirjoittaa? Voit olettaa, että n on
 * alle miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=46
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task46 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', 0);
        $output = [0];
        $i      = 1;

        while ($output[$i - 1] < $number) { // Construct number sequence
            $output[$i] = $i + $output[$i - 1];
            $i++;
        }

        $index = end($output) < $number ? ($i - 1) : $i; // Get the last index where number fits.

        return $index % 2 === 0 ? 'A' : 'B';
    }
}
