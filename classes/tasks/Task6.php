<?php
namespace classes\tasks;

/**
 * Implementation of Task 6
 *
 * Description:
 *
 * Jos vuosi on karkausvuosi, siinä on 366 päivää tavallisen 365:n sijasta. Vuosi on karkausvuosi, jos se on jaollinen
 * 4:llä. Kuitenkin jos vuosi on jaollinen 100:lla, se on karkausvuosi vain, jos se on jaollinen myös 400:lla.
 * Esimerkiksi vuodet 2009, 2010 ja 2011 eivät ole karkausvuosia mutta vuosi 2012 on. Vastaavasti vuodet 2100, 2200 ja
 * 2300 eivät ole karkausvuosia mutta vuosi 2400 on.
 *
 * Tehtävänä on tarkistaa, onko annettu vuosi karkausvuosi. Voit olettaa, että vuosi on välillä 1600–3000.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=6
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task6 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $year = (int)$this->request->getParam('vuosi', 0);

        return (($year % 4 === 0 && $year % 100 !== 0) || ($year % 100 === 0 && $year % 400 === 0)) ? 1 : 0;
    }
}
