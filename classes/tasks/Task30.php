<?php
namespace classes\tasks;

/**
 * Implementation of Task 30
 *
 * Description:
 *
 * Tarkastellaan seuraavaa algoritmia:
 *
 * A. Valitaan jokin kokonaisluku n.
 * B. Jos n on parillinen, jaetaan se kahdella. Jos taas n on pariton, kerrotaan se kolmella ja lisätään tulokseen yksi.
 * C. Jos n on yksi, lopetetaan. Muuten palataan kohtaan B.
 *
 * Esimerkiksi jos valitaan aluksi n = 6, algoritmi toimii seuraavasti:
 *
 * 6 / 2 = 3
 * 3 * 3 + 1 = 10
 * 10 / 2 = 5
 * 5 * 3 + 1 = 16
 * 16 / 2 = 8
 * 8 / 2 = 4
 * 4 / 2 = 2
 * 2 / 2 = 1
 *
 * Tässä algoritmi suoritti kahdeksan kertaa vaiheen B, ennen kuin päästiin lukuun yksi. Tehtävänä on selvittää
 * annetulla n:n arvolla, kuinka monta kertaa algoritmi suorittaa vaiheen B. Voit olettaa, että algoritmi suorittaa
 * vaiheen B korkeintaan tuhat kertaa ja n on joka vaiheessa korkeintaan miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=30
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task30 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', 0);

        $i = 0;

        while ($number !== 1 || $i === 0) {
            $number = ($number % 2 === 0) ? ($number / 2) : (($number * 3) + 1);
            $i++;
        }

        return $i;
    }
}
