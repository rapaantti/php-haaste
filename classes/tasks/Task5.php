<?php
namespace classes\tasks;

/**
 * Implementation of Task 5
 *
 * Description:
 *
 * Luvuista 3, 5, 2, 1 ja 3 pienin on 1 ja suurin on 5. Tehtävänä on etsiä pienin ja suurin luku annetuista luvuista.
 * Voit olettaa, että lukuja on korkeintaan sata ja jokainen luku on positiivinen kokonaisluku ja alle miljoona.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=5
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task5 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $numbersString = $this->request->getParam('luvut', '');
        $numbers = explode('|', $numbersString);

        return min($numbers) . ' ' . max($numbers);
    }
}
