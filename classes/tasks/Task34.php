<?php
namespace classes\tasks;

/**
 * Implementation of Task 34
 *
 * Description:
 *
 * Merkkijonoa laajennetaan seuraavasti:
 *
 * 1. A
 * 2. BAB
 * 3. CBCACBC
 * 4. DCDBDCDADCDBDCD
 * 5. ...
 *
 * Siis jokaisessa vaiheessa merkkijonon päihin ja kaikkiin väleihin lisätään uusi kirjain. Tehtävänä on selvittää näin
 * saatava merkkijono vaiheessa n. Merkkijono kasvaa nopeasti, joten voit olettaa, että n on korkeintaan 10.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=34
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task34 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', 0);
        $charCode = 66; //  ASCII value for B.
        $maxValue = $charCode + $number - 1;
        $output = ['A'];

        for($i = $charCode; $i < $maxValue; $i++) {
            $output = $this->construct($output, chr($i));
        }

        return implode('', $output);
    }

    /**
     * Constructs character array.
     *
     * @param array     $input
     * @param string    $character
     *
     * @return array
     */
    private function construct(array $input, string $character): array
    {
        $output = [];

        foreach ($input as $value) {
            $output[] = $character;
            $output[] = $value;
        }

        $output[] = $character;

        return $output;
    }
}
