<?php
namespace classes\tasks;

/**
 * Implementation of Task 11
 *
 * Description:
 *
 * Tehtävänä on tulostaa kertotaulu, jossa vasen sarake sisältää luvut 1–n ja ylin rivi sisältää luvut 1–m.
 * Voit olettaa, että kertotaulussa on korkeintaan tuhat lukua.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=11
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task11 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $rows = (int)$this->request->getParam('n', 0);
        $columns    = (int)$this->request->getParam('m', 0);

        $output = '';

        for ($i = 1; $i <= $rows; $i++) {
           for ($j = 1; $j <= $columns; $j++) {
               $output .= $i * $j;

               if ($j < $columns) {
                   $output .= ' ';
               }
           }

           $output .= '<br>';
        }


        return $output;
    }
}
