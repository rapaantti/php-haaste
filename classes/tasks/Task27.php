<?php
namespace classes\tasks;

/**
 * Implementation of Task 27
 *
 * Description:
 *
 * Sanasta SUORAKULMIO saadaan sana SALI poistamalla osa kirjaimista. Tehtävänä on selvittää, voiko sanan muuttaa
 * toiseksi poistamalla siitä kirjaimia. Jäljelle jäävien kirjainten järjestys täytyy säilyttää ennallaan.
 * Voit olettaa, että molemmat sanat muodostuvat kirjaimista A–Z ja kummassakin sanassa on enintään sata kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=27
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task27 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word1 = $this->request->getParam('sana1', '');
        $word2 = $this->request->getParam('sana2', '');

        $characters1 = str_split($word1);
        $characters2 = str_split($word2);

        $count2 = count($characters2);

        $j = 0;
        foreach ($characters1 as $character) {
            if (isset($characters2[$j]) && $character === $characters2[$j]) {
                $j++;
            }
        }

        return ($j === $count2) ? 1 : 0;
    }
}
