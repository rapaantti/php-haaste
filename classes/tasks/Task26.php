<?php
namespace classes\tasks;

/**
 * Implementation of Task 26
 *
 * Description:
 *
 * Sanan anagrammi saadaan järjestämällä sanan kirjaimet uudestaan. Esimerkiksi sanan MIKROSEKUNTI anagrammi on
 * SOINTUMERKKI. Tehtävänä on tarkistaa, ovatko kaksi sanaa toistensa anagrammeja. Voit olettaa, että sanat
 * muodostuvat kirjaimista A–Z ja kummassakin sanassa on korkeintaan 50 kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=26
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task26 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word1 = $this->request->getParam('sana1', '');
        $word2 = $this->request->getParam('sana2', '');

        $characters1 = str_split($word1);
        $characters2 = str_split($word2);

        sort($characters1);
        sort($characters2);

        return $characters1 === $characters2 ? 1 : 0;
    }
}
