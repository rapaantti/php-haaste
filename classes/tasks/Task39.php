<?php
namespace classes\tasks;

/**
 * Implementation of Task 39
 *
 * Description:
 *
 * Seuraavassa on lukuspiraali, jonka koko on 4 x 4 ruutua.
 *
 * Suuremmat lukuspiraalit muodostetaan vastaavasti kiertäen vuorotellen ruudukkoa myötäpäivään ja vastapäivään.
 * Tehtävänä on tulostaa lukuspiraali, jonka koko on n x n ruutua. Voit olettaa, että n on korkeintaan 15.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=39
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task39 extends TaskBase
{
    /**#@+
     * Direction constants
     */
    const DIRECTION_CLOCKWISE         = 1;
    const DIRECTION_COUNTER_CLOCKWISE = 2;
    /**#@-*/

    /**
     * Method that executes class task. Method returns result as string.
     *
     * @todo Shitty solution. Refactor this.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = $this->request->getParam('n', 0);
        $array  = [];

        // Initial values
        $k   = 1;
        $i   = 0;
        $j   = 0;
        $max = 1;
        $direction = self::DIRECTION_CLOCKWISE;

        while($k <= $number * $number) {
            $array[$i][$j] = $k++;

            if ($direction === self::DIRECTION_CLOCKWISE) {
                if ($i !== $max && $j !== $max)  {
                    $j++;
                    continue;
                }

                if ($j === $max && $i === $max) {
                    $j--;
                    continue;
                }

                if ($j === 0 && $i === $max) {
                    $i++;
                    $direction = self::DIRECTION_COUNTER_CLOCKWISE;
                    $max++;
                    continue;
                }

                if ($i === $max && $j !== $max) {
                    $j--;
                }

                if ($j === $max && $i !== $max) {
                    $i++;
                    continue;
                }
            } else {
                if ($i === $max && $j !== $max) {
                    $j++;
                    continue;
                }

                if ($i === $max && $j === $max) {
                    $i--;
                    continue;
                }

                if ($i === 0 && $j === $max) {
                    $j++;
                    $direction = self::DIRECTION_CLOCKWISE;
                    $max++;
                    continue;
                }

                if ($j === $max && $i !== $max) {
                    $i--;
                    continue;
                }
            }
        }

        $output = '';

        for ($i = 0; $i < $number; $i++) {
            for ($j = 0; $j < $number; $j++) {
                $output .= $array[$i][$j] . ' ';
            }

            $output .= '<br>';
        }

        return $output;
    }
}
