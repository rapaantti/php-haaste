<?php
namespace classes\tasks;

/**
 * Implementation of Task 40
 *
 * Description:
 *
 * Kalle piirtää ensin neliön, jonka pinta-ala on 1. Sitten hän piirtää kuvion yläpuolelle neliön, jonka leveys on sama
 * kuin kuvion leveys. Sitten hän piirtää kuvion vasemmalle puolelle neliön, jonka korkeus on sama kuin kuvion korkeus.
 * Kalle jatkaa samoin kuvion alapuolelle ja oikealle puolelle ja sitten taas yläpuolelle jne. koko ajan vastapäivään.
 *
 * Jos kirjaimet A, B, C, ... tarkoittavat neliöitä, alkaa siis muodostua seuraava kuvio:
 *
 * Yllä oleva kuvio on muodostunut, kun Kalle on piirtänyt seitsemän neliötä. Kuvion pinta-ala on 273, koska sen leveys
 * on 21 ja korkeus 13. Mikä on kuvion pinta-ala, kun Kalle on piirtänyt n neliötä? Voit olettaa, että pinta-ala on
 * alle miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=40
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task40 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = $this->request->getParam('n', 0);

        $sequence = $this->fibonacci($number);
        $areas    = $this->calculateArea($sequence);

        return array_sum($areas);
    }

    /**
     * Calculates fibonacci sequence to given number.
     *
     * @param int $number
     *
     * @return array
     */
    private function fibonacci(int $number): array
    {
        $output = [1];

        for ($i = 1; $i < $number; $i++) {
            $output[$i] = $output[$i - 1] + $output[$i - 2];
        }

        return $output;
    }

    /**
     * Calculates area for every array value.
     *
     * @param array $sequence
     *
     * @return array
     */
    private function calculateArea(array $sequence): array
    {
        $iterator = function($value) {
            return $value * $value;
        };

        return array_map($iterator, $sequence);
    }
}
