<?php
namespace classes\tasks;

/**
 * Implementation of Task 2
 *
 * Description:
 *
 * Järjestelmän käyttäjän tunnus on admin ja salasana on sala123. Tehtävänä on tarkistaa, ovatko käyttäjän tunnus ja
 * salasana oikein. Jos tunnus ja salasana ovat oikein, skriptin täytyy tulostaa viesti OK. Muuten skriptin täytyy
 * tulostaa viesti VIRHE.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=2
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task2 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $username = $this->request->getParam('tunnus', '');
        $password = $this->request->getParam('salasana', '');

        return ($username === 'admin' && $password === 'sala123') ? 'OK' : 'VIRHE';
    }
}
