<?php
namespace classes\tasks;

/**
 * Implementation of Task 35
 *
 * Description:
 *
 * Tehtävänä on etsiä pisin merkkijonon osana oleva palindromi. Esimerkiksi pisin merkkijonon MAALAISTALO osana oleva
 * palindromi on ALA, jossa on kolme kirjainta. Vastaavasti pisin merkkijonon VAIHTAVAT osana oleva palindromi on TAVAT,
 * jossa on viisi kirjainta.
 *
 * Voit olettaa, että merkkijono muodostuu kirjaimista A–Z ja siinä on korkeintaan tuhat kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=35
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task35 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $string = $this->request->getParam('mjono', '');

        if ($string === strrev($string)) { // Whole string is palindrome so return its length.
            return strlen($string);
        }

        $count = strlen($string);
        $longestWord = '';

        for($i = 0; $i < $count; $i++) {
            for($j = 0; $j < $count; $j++) {
                $word = substr($string, $i, $j);

                if ($this->isPalindrome($word) && strlen($longestWord) < strlen($word)) {
                    $longestWord = $word;
                }
            }
        }

        return strlen($longestWord);
    }

    /**
     * Helper function to check whether string is palindrome or not.
     *
     * @param string $string
     *
     * @return bool
     */
    private function isPalindrome(string $string): bool
    {
        return ($string === strrev($string));
    }
}
