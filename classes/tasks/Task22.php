<?php
namespace classes\tasks;

/**
 * Implementation of Task 22
 *
 * Description:
 *
 * Taikaneliö on n x n -ruudukko, johon on sijoitettu luvut 1–n2. Kun taikaneliön minkä tahansa rivin, sarakkeen tai
 * lävistäjän luvut lasketaan yhteen, tulos on sama. Tässä on yksi 4 x 4 -taikaneliö:
 *
 * +--+--+--+--+
 * | 1|15|14| 4|
 * +--+--+--+--+
 * |12| 6| 7| 9|
 * +--+--+--+--+
 * | 8|10|11| 5|
 * +--+--+--+--+
 * |13| 3| 2|16|
 * +--+--+--+--+
 *
 * Jokaisen rivin, sarakkeen ja lävistäjän lukujen summa on 34. Esimerkiksi toisen rivin summa on 12 + 6 + 7 + 9 = 34 ja
 * vasemmasta alakulmasta alkavan lävistäjän summa on 13 + 10 + 7 + 4 = 34.
 *
 * Tehtävänä on tarkistaa, onko annettu 4 x 4 -taikaneliö kelvollinen eli sisältääkö se kaikki luvut 1–16 ja onko
 * jokaisen rivin, sarakkeen ja lävistäjän lukujen summa sama.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=22
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task22 extends TaskBase
{
    /**
     * Default grid dimension
     */
    const DIMENSION = 4;
    /**
     * Default sum (which is valid)
     */
    const DEFAULT_SUM = 34;

    /**
     * How many rows we check
     */
    const ROWS = 6;

    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $numbersString = $this->request->getParam('nelio', 0);
        $numbers = explode('|', $numbersString);

        $isValid = count(array_diff(array_keys(array_fill(1, 16, 1)), $numbers)) === 0;

        if (!$isValid) {
            return 0;
        }

        $output = [];
        $k = 0;
        $diagonalSum = 0;
        $diagonalSum2 = 0;
        $booleans = [];

        for ($i = 0; $i < self::DIMENSION; $i++) {
            for ($j = 0; $j < self::DIMENSION; $j++) {
                $output[$i][$j] = (int)$numbers[$k++];

                if ($i === $j) { // Diagonal sum y = -x
                    $diagonalSum += $output[$i][$j];
                } else if ( $j === (self::DIMENSION - 1) - $i) { // Diagonal sum y = x
                    $diagonalSum2 += $output[$i][$j];
                }
            }

            // Sum rows
            $booleans[] = array_sum($output[$i]) === self::DEFAULT_SUM;
        }

        $booleans[] = $diagonalSum === self::DEFAULT_SUM;
        $booleans[] = $diagonalSum2 === self::DEFAULT_SUM;

        return count(array_filter($booleans)) === self::ROWS ? 1 : 0;
    }
}

