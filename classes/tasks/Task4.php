<?php
namespace classes\tasks;

/**
 * Implementation of Task 4
 *
 * Description:
 *
 * Tässä tehtävässä täytyy laskea yhteen annetut luvut. Esimerkiksi jos luvut ovat 3, 8 ja 4, niiden summa on 15.
 * Luvut ovat positiivisia kokonaislukuja, ja niiden summa on alle miljoona.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=4
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task4 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $numbersString = $this->request->getParam('luvut', '');

        return array_sum(explode('|', $numbersString));
    }
}
