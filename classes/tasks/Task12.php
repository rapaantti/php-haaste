<?php
namespace classes\tasks;

/**
 * Implementation of Task 12
 *
 * Description:
 *
 * Lukujen 5, 3, 8, 2, 5, 7 ja 1 joukossa on kaksi kertaa sama luku, nimittäin luku 5. Tehtävänä on tutkia,
 * onko lukujoukossa monta kertaa sama luku. Voit olettaa, että lukuja on korkeintaan 100 ja ne ovat kokonaislukuja
 * välillä 0–1000000.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=12
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task12 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $numbersString = $this->request->getParam('luvut', '');

        $numbers = explode('|', $numbersString);

        $output = [];

        foreach ($numbers as $number) {
            if (!isset($output[$number])) {
                $output[$number] = 0;
            }

            $output[$number]++;
        }

        return max($output) - 1;
    }
}
