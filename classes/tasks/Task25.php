<?php
namespace classes\tasks;

/**
 * Implementation of Task 25
 *
 * Description:
 *
 * Luvuista 2, 8, 13, 5, 7 ja 10 lähimpänä toisiaan ovat luvut 7 ja 8, sillä niiden ero on vain 1. Tehtävänä on
 * selvittää, mikä on kahden lähimmän luvun ero annetussa lukujoukossa. Voit olettaa, että lukuja on korkeintaan 100 ja
 * luvut ovat positiivisia kokonaislukuja välillä 0–1000000.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=25
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task25 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $numbersString = $this->request->getParam('luvut', '');
        $numbers = array_map('intval', explode('|', $numbersString));
        $count = count($numbers);
        $distances = [];

        for ($i = 0; $i < $count; $i++) {
            for ($j = 0; $j < $count; $j++) {
                if ($j === $i) { // Skip distance to number itself
                    continue;
                }

                $distances[] = abs($numbers[$i] - $numbers[$j]);
            }
        }

        return min($distances);
    }
}
