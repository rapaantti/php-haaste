<?php
namespace classes\tasks;

/**
 * Implementation of Task 20
 *
 * Description:
 *
 * Kaksi ensimmäistä Fibonaccin lukua ovat 0 ja 1. Tämän jälkeen seuraava Fibonaccin luku saadaan laskemalla yhteen
 * kaksi edellistä. Fibonaccin luvut ovat siis: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...
 *
 * Tehtävänä on laskea n:s Fibonaccin luku. Voit olettaa, että vastaus on alle miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=20
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task20 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', 0);

        $output = [0, 1];

        for ($i = 1; $i < $number - 1; $i++) {
            $output[$i + 1] = $output[$i - 1] + $output[$i];
        }

        return $output[$number - 1];
    }
}
