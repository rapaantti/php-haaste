<?php
namespace classes\tasks;

/**
 * Implementation of Task 14
 *
 * Description:
 *
 * Uolevilla on lottokuponki, jossa on numerot 3, 6, 7, 10, 23, 26 ja 36. Lottokone taas arpoo numerot
 * 5, 7, 14, 18, 26, 31 ja 38. Tämä tarkoittaa, että Uolevin kupongissa on kaksi numeroa oikein, nimittäin
 * numerot 7 ja 26.
 *
 * Tehtävänä on selvittää, kuinka monta numeroa on oikein, kun tiedossa ovat lottokupongin numerot sekä lottokoneen
 * arpomat numerot. Numeroita on seitsemän ja ne ovat välillä 1–39. Voit olettaa, että numerot ilmoitetaan
 * pienimmästä suurimpaan.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=14
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task14 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $numberString1 = $this->request->getParam('num1', '');
        $numberString2 = $this->request->getParam('num2', '');

        $numbers1 = array_map('intval', explode('|', $numberString1));
        $numbers2 = array_map('intval', explode('|', $numberString2));

        return 7 - count(array_diff($numbers1, $numbers2));
    }
}
