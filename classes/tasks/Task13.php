<?php
namespace classes\tasks;

/**
 * Implementation of Task 13
 *
 * Description:
 *
 * Sana on palindromi, jos se on sama alusta loppuun ja lopusta alkuun luettuna. Esimerkiksi sanat ENNE ja SYTYTYS
 * ovat palindromeja. Tehtävänä on tarkistaa, onko annettu sana palindromi. Voit olettaa, että sana muodostuu
 * kirjaimista A–Z ja siinä on korkeintaan sata kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=13
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task13 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word = $this->request->getParam('sana', '');
        $wordArray = array_map('strtoupper', str_split($word));

        return $wordArray === array_reverse($wordArray) ? 1 : 0;
    }
}
