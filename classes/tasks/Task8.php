<?php
namespace classes\tasks;

/**
 * Implementation of Task 8
 *
 * Description:
 *
 * Sanassa SYYSMAISEMA on kuusi vokaalia ja viisi konsonanttia. Tehtävänä on selvittää, kuinka monta vokaalia ja
 * konsonanttia on annetussa sanassa. Voit olettaa, että sana muodostuu kirjaimista A–Z ja siinä on korkeintaan
 * sata kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=8
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task8 extends TaskBase
{
    /**
     * @var array Vocals in array
     */
    private static $vocals = array(
        'A',
        'E',
        'I',
        'O',
        'U',
        'Y',
        'Ä',
        'Ö'
    );

    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word = $this->request->getParam('sana', '');

        $count = strlen($word);

        $vocals = 0;
        $consonants = 0;

        for ($i = 0; $i < $count; $i++) {
            in_array(strtoupper($word[$i]), self::$vocals, true) ? $vocals++ : $consonants++;
        }

        return $vocals . ' ' . $consonants;
    }
}
