<?php
namespace classes\tasks;

/**
 * Implementation of Task 45
 *
 * Description:
 *
 * Suomalainen henkilötunnus on muotoa PPKKVVMNNNT, jossa PPKKVV on henkilön syntymäaika, M on vuosisatamerkki, NNN on
 * tunnusnumero ja T on tarkistusmerkki. Vuosisatamerkki on +, - tai A sen mukaan, onko henkilö syntynyt 1800-, 1900-
 * vai 2000-luvulla. Kolminumeroinen tunnusnumero erottaa samana päivänä syntyneet henkilöt toisistaan, ja se on
 * miehillä pariton ja naisilla parillinen.
 *
 * Tarkistusmerkki saadaan selville jakamalla luku PPKKVVNNN (henkilötunnus ilman välimerkkiä ja tarkistusmerkkiä)
 * 31:llä ja valitsemalla jakojäännöksen mukaan merkki seuraavasta taulukosta:
 *
 * Esimerkiksi jos henkilötunnus on 170374-322V, henkilö on syntynyt 17.3.1974 ja naispuolinen. Tarkistusmerkki on V,
 * koska jos luku 170374322 jaetaan 31:llä, jakojäännös on 27 ja taulukossa luvun 27 kohdalla on merkki V.
 *
 * Tehtävänä on tarkistaa, onko annetun henkilötunnuksen tarkistusmerkki oikein. Voit olettaa, että henkilötunnus on
 * muuten oikeanmuotoinen.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=45
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task45 extends TaskBase
{
    /**
     * Check marks for modulo results.
     *
     * @var array
     */
    private static $checkMarks = [
        '0', '1', '2', '3', '4', '5',
        '6', '7', '8', '9', 'A', 'B',
        'C', 'D', 'E', 'F', 'H', 'J',
        'K', 'L', 'M', 'N', 'P', 'R',
        'S', 'T', 'U', 'V', 'W', 'X',
        'Y'
    ];

    /**
     * Separators with year prefix in array.
     *
     * @var array
     */
    private static $separators = [
        '+' => 18,
        '-' => 19,
        'A' => 20
    ];

    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $ssn = $this->request->getParam('hetu', '');

        return $this->checkSSN($ssn) ? 1 : 0;
    }

    /**
     * Method checks whether given social security number is valid.
     *
     * @param string $ssn
     *
     * @return bool
     */
    private function checkSSN(string $ssn): bool
    {
        $day        = substr($ssn, 0, 2);
        $month      = substr($ssn, 2, 2);
        $year       = substr($ssn, 4, 2);
        $separator  = $ssn[6];
        $checksum   = substr($ssn, 7, 3);
        $modulo     = (int)($day . $month . $year . $checksum) % 31;
        $checkMark  = self::$checkMarks[$modulo];
        $yearPrefix = self::$separators[$separator];

        $dateObj = \DateTime::createFromFormat('dmY', $day . $month . $yearPrefix . $year);

        if (!$dateObj ) { // Check date validity
            return false;
        }

        $ssnTmp = $dateObj->format('dmy') . $separator . $checksum . $checkMark;

        return $ssnTmp === $ssn;
    }
}
