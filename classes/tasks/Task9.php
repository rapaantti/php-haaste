<?php
namespace classes\tasks;

/**
 * Implementation of Task 9
 *
 * Description:
 *
 * Vokaalijono on joukko sanan peräkkäisiä vokaaleita. Sanan PUUAINES pisin vokaalijono on UUAI. Tehtävänä on selvittää,
 * kuinka pitkä on annetun sanan pisin vokaalijono. Voit olettaa, että sana muodostuu kirjaimista A–Z ja
 * siinä on korkeintaan sata kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=9
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task9 extends TaskBase
{
    /**
     * @var array Vocals in array
     */
    private static $vocals = array(
        'A',
        'E',
        'I',
        'O',
        'U',
        'Y',
        'Ä',
        'Ö'
    );

    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word = $this->request->getParam('sana', '');

        $count = strlen($word);

        $max = 0;
        $vocals = 0;

        for ($i = 0; $i < $count; $i++) {
            if (in_array(strtoupper($word[$i]), self::$vocals, true)) {
                $vocals++;

                if ($vocals > $max) {
                    $max = $vocals;
                }
            } else {
                $vocals = 0;
            }

        }

        return $max;
    }
}
