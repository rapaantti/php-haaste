<?php
namespace classes\tasks;

/**
 * Implementation of Task 23
 *
 * Description:
 *
 * Palindromiluvun numerot ovat samat alusta loppuun ja lopusta alkuun luettuina. Esimerkiksi luvut 1441 ja 73237 ovat
 * palindromilukuja. Luvun voi yrittää muuttaa palindromiluvuksi lisäämällä siihen toistuvasti luku, joka sisältää
 * luvun numerot käänteisessä järjestyksessä.
 *
 * Esimerkiksi jos aloitetaan luvusta 19, siihen lisätään ensin 91, jolloin tulos on 110. Tähän taas lisätään 011 eli
 * 11, jolloin tulos on 121 ja on saatu aikaan palindromiluku.
 *
 * Tehtävänä on selvittää, kuinka monta lisäystä tarvitaan, ennen kuin tietystä luvusta tulee palindromiluku.
 * Voit olettaa, että aloitusluku on positiivinen kokonaisluku ja alle miljoona, lisäyksiä tarvitaan korkeintaan
 * tuhat ja lopputulos on alle miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=23
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task23 extends TaskBase
{

    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', 0);

        $reverseNumber = (int)strrev((string)$number);
        $count = 0;

        while ($number !== $reverseNumber) {
            $number += $reverseNumber;
            $reverseNumber = (int)strrev((string)$number);
            $count++;
        }

        return $count;
    }
}

