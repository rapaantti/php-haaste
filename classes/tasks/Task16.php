<?php
namespace classes\tasks;

/**
 * Implementation of Task 16
 *
 * Description:
 *
 * Luvut 4, 7, 5, 9, ja 10 eivät ole järjestyksessä, koska luku 7 tulee ennen lukua 5. Tehtävänä on selvittää, ovatko
 * annetut luvut järjestyksessä pienimmästä suurimpaan. Voit olettaa, että lukuja on korkeintaan sata ja luvut ovat
 * positiivisia kokonaislukuja ja pienempiä kuin miljoona.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=16
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task16 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $numberString = $this->request->getParam('luvut', '');

        $numbers = array_map('intval', explode('|', $numberString));
        $sortedNumbers = $numbers;

        sort($sortedNumbers);

        return $numbers === $sortedNumbers ? 1 : 0;
    }
}
