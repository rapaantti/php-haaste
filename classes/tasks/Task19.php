<?php
namespace classes\tasks;

/**
 * Implementation of Task 19
 *
 * Description:
 *
 * Luvun 81 neliöjuuri 9 on kokonaisluku, mutta luvun 82 neliöjuuri 9,055385... ei ole kokonaisluku. Tehtävänä on
 * selvittää, onko annetun kokonaisluvun neliöjuuri kokonaisluku. Voit olettaa, että luku on korkeintaan miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=19
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task19 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('luku', '');

        $squareRoot = sqrt($number);

        return $squareRoot === floor($squareRoot) ? 1 : 0;
    }
}
