<?php
namespace classes\tasks;

/**
 * Implementation of Task 42
 *
 * Description:
 *
 * Ruudukossa on n riviä ja m saraketta. Sen ruudut on numeroitu vasemmalta oikealle ja ylhäältä alas kokonaisluvuin
 * 1:stä aloittaen. Tehtävänä on selvittää, missä ruudukon kohdassa on tietty luku. Voit olettaa, että n ja m ovat
 * korkeintaan 1000.
 *
 * Esimerkiksi jos n on 3 ja m on 5, ruudukko näyttää seuraavalta:
 *
 * Nyt luku 8 on ruudukossa rivillä 2 sarakkeessa 3.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=42
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task42 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $height         = (int)$this->request->getParam('n', 0);
        $width          = (int)$this->request->getParam('m', 0);
        $expectedNumber = (int)$this->request->getParam('x', 0);

        $number = 1;

        for ($i = 1; $i <= $height; $i++) {
            for ($j = 1; $j <= $width; $j++) {
                if ($number === $expectedNumber) {
                    return $i . ' ' . $j;
                }

                $number++;
            }
        }

        return '';
    }
}
