<?php
namespace classes\tasks;

/**
 * Implementation of Task 41
 *
 * Description:
 *
 * Kalle piirtää ensin neliön, jonka pinta-ala on 1. Sitten hän piirtää kuvion yläpuolelle neliön, jonka leveys on sama
 * kuin kuvion leveys. Sitten hän piirtää kuvion vasemmalle puolelle neliön, jonka korkeus on sama kuin kuvion korkeus.
 * Kalle jatkaa samoin kuvion alapuolelle ja oikealle puolelle ja sitten taas yläpuolelle jne. koko ajan vastapäivään.
 *
 * Jos kirjaimet A, B, C, ... tarkoittavat neliöitä, alkaa siis muodostua seuraava kuvio:
 *
 * Yllä oleva kuvio on muodostunut, kun Kalle on piirtänyt seitsemän neliötä. Kuvion pinta-ala on 273, koska sen leveys
 * on 21 ja korkeus 13. Mikä on kuvion pinta-ala, kun Kalle on piirtänyt n neliötä? Voit olettaa, että pinta-ala on
 * alle miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=41
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task41 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $height = $this->request->getParam('n', 0);
        $width  = $this->request->getParam('m', 0);
        $row    = $this->request->getParam('r', 0);
        $column = $this->request->getParam('s', 0);

        $array  = [];
        $number = 1;

        for ($i = 1; $i <= $height; $i++) {
            for ($j = 1; $j <= $width; $j++) {
                $array[$i][$j] = $number++;
            }
        }

        return $array[$row][$column];
    }
}
