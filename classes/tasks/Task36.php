<?php
namespace classes\tasks;

/**
 * Implementation of Task 36
 *
 * Description:
 *
 * Kalle laittaa ensimmäisenä päivänä hiekalle yhden neliön muotoisen kiven. Toisena päivänä hän laittaa kiven ympärille
 * uuden kerroksen kiviä. Kolmantena päivänä hän laittaa kivien ympärille jälleen uuden kerroksen kiviä. Sama toistuu
 * muinakin päivinä. Kivistä muodostuu seuraava kuvio:
 *
 * Neljäntenä päivänä Kalle lisää hiekalle 24 kiveä, jotka on merkitty kuvassa kirjaimella D. Kuinka monta kiveä Kalle
 * lisää hiekalle päivänä n? Voit olettaa, että n on korkeintaan tuhat.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=36
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task36 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', '');
        $width  = ($number + ($number - 1));

        return (($width * $width) - (($width - 2) * ($width - 2)));
    }
}
