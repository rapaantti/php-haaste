<?php
namespace classes\tasks;

/**
 * Implementation of Task 28
 *
 * Description:
 *
 * Kun sanaa kierretään vasemmalle, sen ensimmäinen kirjain siirtyy viimeiseksi. Kun sanaa kierretään oikealle, sen
 * viimeinen kirjain siirtyy ensimmäiseksi. Esimerkiksi jos sanaa AVARUUS kierretään kolmesti vasemmalle,
 * lopputulos on RUUSAVA.
 *
 * Tehtävänä on selvittää, miten sana muuttuu, kun sitä kierretään tietyn verran tiettyyn suuntaan. Voit olettaa, että
 * sanassa on korkeintaan sata kirjainta ja sitä kierretään korkeintaan tuhat kertaa.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=28
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task28 extends TaskBase
{
    /**
     * Constant for left direction
     */
    const DIRECTION_LEFT = 'V';

    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word      = $this->request->getParam('sana', '');
        $direction = $this->request->getParam('suunta', 'V');
        $amount    = (int)$this->request->getParam('maara', 0);


        $characters = str_split($word);
        $count      = count($characters);
        $output     = [];

        foreach ($characters as $key => $character) {
            $newKey = ($direction === self::DIRECTION_LEFT ? $key - $amount : $key + $amount) % $count;

            if ($newKey < 0) { // Sorry, php's modulo returns negative values.
                $newKey = $count + $newKey;
            }

            $output[$newKey] = $character;
        }

        ksort($output); // Sort using keys.

        return implode('', $output);
    }
}
