<?php
namespace classes\tasks;

/**
 * Implementation of Task 15
 *
 * Description:
 *
 * Toteuta äänestyskone neljälle äänestäjälle. Yksi äänestäjistä on puheenjohtaja, jonka ääni ratkaisee, mikäli äänet
 * menevät tasan.
 *
 * Esimerkki 1: Puheenjohtaja äänestää asian puolesta ja muut äänestävät asiaa vastaan. Äänestystulos on asiaa vastaan,
 * koska kolme henkilöä äänestää asiaa vastaan ja vain yksi sen puolesta.
 *
 * Esimerkki 2: Puheenjohtaja ja 2. äänestäjä äänestävät asian puolesta ja 1. äänestäjä ja 3. äänestäjä äänestävät asiaa
 * vastaan. Äänestystulos on asian puolesta, koska äänet menevät tasan ja puheenjohtaja äänestää asian puolesta.
 *
 * Syötteessä numero 1 tarkoittaa asian puolesta ja numero 0 asiaa vastaan.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=15
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task15 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $voterPj = (int)$this->request->getParam('pj', 0);
        $voterA  = (int)$this->request->getParam('a', 0);
        $voterB  = (int)$this->request->getParam('b', 0);
        $voterC  = (int)$this->request->getParam('c', 0);

        $votes[] = $voterPj;
        $votes[] = $voterA;
        $votes[] = $voterB;
        $votes[] = $voterC;

        $voteSum = array_sum($votes);
        $average = (int)(count($votes) / 2);

        return ($voteSum !== $average ? array_sum($votes) > $average : $voterPj) ? 1: 0;
    }
}
