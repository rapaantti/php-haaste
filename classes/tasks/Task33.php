<?php
namespace classes\tasks;

/**
 * Implementation of Task 33
 *
 * Description:
 *
 * Pascalin kolmion reunat muodostuvat luvuista 1 ja kolmion sisällä kahden luvun alapuolella on niiden summa. Pascalin
 * kolmion ensimmäiset rivit ovat siis seuraavat:
 *
 *         1
 *       1   1
 *     1   2   1
 *   1   3   3   1
 * 1   4   6   4   1
 *
 * Pascalin kolmion viidennellä rivillä ovat luvut 1, 4, 6, 4 ja 1. Tehtävänä on selvittää, mitkä luvut ovat Pascalin
 * kolmion rivillä n. Voit olettaa, että jokainen rivillä oleva luku on korkeintaan miljoona.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=33
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task33 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = $this->request->getParam('n', 0);

        $output = [1];

        for ($i = 0; $i < ($number - 1); $i++) {
            $output[] = $output[$i] * (($number - 1) - $i) / ($i + 1);
        }

        return implode(' ', $output);
    }
}
