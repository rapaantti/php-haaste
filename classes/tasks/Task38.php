<?php
namespace classes\tasks;

/**
 * Implementation of Task 38
 *
 * Description:
 *
 * Tässä tehtävässä tutkitaan vuosien 1800–2200 päivämääriä. Päivämäärä on kelvollinen, jos siinä on ensin luku
 * väliltä 1–31 (päivä), sitten piste, sitten luku väliltä 1–12 (kuukausi), sitten piste ja lopuksi luku väliltä
 * 1800–2200 (vuosi). Lisäksi päivä ei saa olla suurempi kuin kuukauden viimeinen päivä
 * (kuukaudessa voi olla alle 31 päivää).
 *
 * Esimerkiksi 10.5.2009 ja 7.10.1867 ovat kelvollisia päivämääriä. Vastaavasti 6.14.2136, 31.6.1950 ja 11.02.1950
 * eivät ole kelvollisia päivämääriä, koska kuukausia on vain 12, kesäkuussa on vain 30 päivää ja luvuissa ei
 * saa olla ylimääräisiä etunollia.
 *
 * Tehtävänä on tarkistaa, onko annettu päivämäärä kelvollinen.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=38
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task38 extends TaskBase
{
    /**
     * Days in month.
     *
     * @var array
     */
    private $daysInMonth = array(
        31, // January
        28, // February
        31, // March
        30, // April
        31, // May
        30, // June
        31, // July
        31, // August
        30, // September
        31, // October
        30, // November
        31  // December
    );
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $dateString = $this->request->getParam('pvm', '');
        $dateParts  = explode('.', $dateString);

        if (count($dateParts) !== 3) { // Validate day, month, year.
            return 0;
        }

        list($day, $month, $year) = $dateParts;

        if ((int)$year < 1800 || (int)$year > 2200) {
            return 0;
        }

        if ((int)$month < 1 || (int)$month > 12 || (int)$month[0] === 0) {
            return 0;
        }

        if ((int)$day < 1 || (int)$day > $this->daysInMonth[$month - 1] || (int)$month[0] === 0) {
            if ((int)$month === 2 && (int)$day <= $this->daysInMonth[$month - 1] + 1 && $this->isLeapYear((int)$year)) {
                return 1;
            }

            return 0;
        }

        return 1;
    }

    /**
     * Function to check whether year is a leap year or not.
     *
     * @param int $year
     *
     * @return bool
     */
    public function isLeapYear(int $year): bool
    {
        return (($year % 4 === 0 && $year % 100 !== 0) || ($year % 100 === 0 && $year % 400 === 0));
    }
}
