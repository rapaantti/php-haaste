<?php
namespace classes\tasks;

/**
 * Implementation of Task 10
 *
 * Description:
 *
 * Luvun n kertoma n! on tulo 1 * 2 * 3 * ... * n. Esimerkiksi 5! = 1 * 2 * 3 * 4 * 5 = 120. Lisäksi on määritelty
 * 0! = 1. Mikä on luvun n kertoma? Voit olettaa, että vastaus on korkeintaan miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=10
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task10 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', 0);

        $output = 1;

        while ($number > 1) {
           $output *= $number--;
        }

        return $output;
    }
}
