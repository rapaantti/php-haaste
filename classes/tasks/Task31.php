<?php
namespace classes\tasks;

/**
 * Implementation of Task 31
 *
 * Description:
 *
 * Keksitkö, mikä logiikka on seuraavassa lukujonossa?
 *
 * 1, 11, 21, 1211, 111221, 312211, 13112221, 1113213211, ...
 *
 * Vastaus: Seuraava luku kertoo, mitä numeroita edellisessä luvussa on. Ensimmäisessä luvussa on yksi ykkönen, joten
 * toinen luku on 11. Toisessa luvussa on kaksi ykköstä, joten kolmas luku on 21. Kolmannessa luvussa on yksi kakkonen
 * ja yksi ykkönen, joten neljäs luku on 1211. Neljännessä luvussa on yksi ykkönen, yksi kakkonen ja kaksi ykköstä,
 * joten viides luku on 111221. Samalla tavalla voi jatkaa loputtomiin.
 *
 * Lukujonon viides luku on 111221. Mikä on lukujonon n:s luku? Voit olettaa, että vastauksessa on korkeintaan sata
 * numeroa.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=31
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task31 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $n = (int)$this->request->getParam('n', 0);

        // Initial data.
        $output = [1];
        $i      = 1;

        while ($i < $n) {
            $numbers = str_split((string)$output[$i - 1]);

            $lastNumber = $numbers[0];
            $count      = count($numbers);
            $amount     = 1;

            for ($j = 1; $j < $count; $j++) {
                if ($lastNumber === $numbers[$j]) {
                    $amount++;
                } else {
                    $output[$i] .= (int)($amount . $lastNumber);
                    $amount      = 1;
                }

                $lastNumber = $numbers[$j];
            }

            $output[$i] .= (int)($amount . $lastNumber);
            $i++;
        }

        return $output[$n - 1];
    }
}
