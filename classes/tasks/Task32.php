<?php
namespace classes\tasks;

/**
 * Implementation of Task 32
 *
 * Description:
 * Kirjainjoukko QKADOKBAPC voidaan esittää selkeämmin A-D,K,O-Q. Tehtävänä on tehdä kirjainjoukolle seuraava muunnos:
 * Kirjaimet ovat aakkosjärjestyksessä, ja kukin kirjain ilmoitetaan vain kerran.
 * Jos kahden kirjaimen välistä puuttuu kirjain, siinä kohdassa on pilkku.
 * Peräkkäisistä kirjaimista ilmoitetaan ensimmäinen ja viimeinen väliviivan kanssa.
 * Käytössä ovat kirjaimet A–Z, ja kirjainjoukossa on enintään sata kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=32
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task32 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $characters = $this->request->getParam('kirj', '');
        $characters = array_unique(str_split($characters));

        sort($characters); // Not in order. :-(

        $characterSeries = [];
        $count = count($characters);
        $j = 0;

        for ($i = 0; $i < $count; $i++) {
            $characterSeries[$j][] = $characters[$i];

            if (ord($characters[$i]) + 1 !== ord($characters[$i + 1])) {
                $j++;
            }
        }

        $count  = count($characterSeries);
        $output = [];

        for ($i = 0; $i < $count; $i++) {
            $first = $characterSeries[$i][0];
            $last  = $characterSeries[$i][count($characterSeries[$i]) - 1];

            if ($first !== $last) {
                $output[] .= $first . '-' . $last;
            } else {
                $output[] = $first;
            }
        }

        return implode(',' , $output);
    }
}
