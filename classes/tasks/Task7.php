<?php
namespace classes\tasks;

/**
 * Implementation of Task 7
 *
 * Description:
 *
 * Jos luvut 1, 2, 3, 4 ja 5 lasketaan yhteen, summa on 15. Vastaavasti jos lukujen neliöt 12, 22, 32, 42 ja 52
 * lasketaan yhteen, summa on 55. Tehtävänä on laskea vastaavat summat, kun luvut ovat 1, 2, 3, ..., n.
 * Voit olettaa, että kumpikin summa on alle miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=7
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task7 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', 0);

        $output = [];

        for ($i = 1; $i <= $number; $i++) {
            $output[] = $i;
        }

        $iterator = function($value) {
            return $value * $value;
        };

        return array_sum($output) . ' ' . array_sum(array_map($iterator, $output));
    }
}
