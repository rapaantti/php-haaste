<?php
namespace classes\tasks;

/**
 * Implementation of Task 44
 *
 * Description:
 *
 * Alkuluku on kokonaisluku, joka on suurempi kuin 1 ja jaollinen vain 1:llä ja itsellään. Esimerkiksi luku 18 ei ole
 * alkuluku, koska se on jaollinen 2:lla ja 9:llä. Sitä vastoin luku 19 on alkuluku, koska se ei ole jaollinen millään
 * luvuista 2–18.
 *
 * Tässä ovat ensimmäiset alkuluvut:
 * 2, 3, 5, 7, 11, 13, 17, 19, 23, 29
 *
 * Tehtävänä on selvittää, onko annettu luku alkuluku. Voit olettaa, että luku on alle miljoona.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=44
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task44 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('luku', 0);

        return (string)$this->isPrime($number) ? 1 : 0;
    }

    /**
     * Check whether given number is a prime number or not.
     *
     * @param int $number
     *
     * @return bool
     */
    private function isPrime(int $number): bool
    {
        for ($i = 2; $i < $number; $i++) {
            if ($number % $i === 0 && $i !== $number) {
                return false;
            }
        }

        return true;
    }
}
