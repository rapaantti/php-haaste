<?php
namespace classes\tasks;

/**
 * Implementation of Task 18
 *
 * Description:
 *
 * Sanassa PAKKAUS on kaksi A:ta, kaksi K:ta, yksi P, yksi S ja yksi U. Tehtävänä on ilmoittaa kaikki sanassa olevat
 * kirjaimet ja niiden esiintymismäärät. Kirjaimet täytyy luetella aakkosjärjestyksessä. Voit olettaa, että sana
 * muodostuu kirjaimista A–Z ja siinä on korkeintaan 100 kirjainta.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=18
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task18 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word = $this->request->getParam('sana', '');

        $count = strlen($word);

        $characters = [];

        for ($i = 0; $i < $count; $i++) {
            $char = strtoupper($word[$i]);

            if (!isset($characters[$char])) {
                $characters[$char] = 0;
            }

            $characters[$char]++;
        }

        $output = '';

        ksort($characters);

        foreach ($characters as $character => $count) {
            $output .= sprintf(
                '%1$s %2$s<br>',
                /** 1 */ $character,
                /** 2 */ $count
            );
        }

        return $output;
    }
}
