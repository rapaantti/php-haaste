<?php
namespace classes\tasks;

/**
 * Implementation of Task 3
 *
 * Description:
 *
 * Luku 12 on parillinen, kun taas luku 11 on pariton. Tehtävänä on selvittää, onko luku parillinen vai pariton.
 * Voit olettaa, että luku on positiivinen kokonaisluku ja korkeintaan miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=3
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task3 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = $this->request->getParam('luku', 0);

        return ($number % 2 === 0) ? 'parillinen' : 'pariton';
    }
}
