<?php
namespace classes\tasks;

/**
 * Implementation of Task 24
 *
 * Description:
 *
 * Tässä tehtävässä tutkitaan sanoja, jotka muodostuvat kirjaimista A–Z. ROT13-salauksessa jokaista sanan kirjainta
 * siirretään 13 askelta eteenpäin aakkosissa:
 *
 * Esimerkiksi sana SALAISUUS muuttuu muotoon FNYNVFHHF. Jos ROT13-salauksen suorittaa kahdesti, sana ei muutu
 * mitenkään, koska kirjaimia A–Z on 26.
 *
 * Tehtävänä on kohdistaa ROT13-salaus annettuun sanaan. Voit olettaa, että sanassa on korkeintaan sata kirjainta.
 *
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=24
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task24 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $word = $this->request->getParam('sana', '');

        return str_rot13($word);
    }
}
