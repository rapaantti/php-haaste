<?php
namespace classes\tasks;

/**
 * Implementation of Task 43
 *
 * Description:
 *
 * Jos tänään on torstai, niin mikä päivä on 20 päivän kuluttua? Vastaus: keskiviikko. Tehtävänä on selvittää, mikä
 * viikonpäivä on annetusta päivästä n päivän kuluttua. Voit olettaa, että n on korkeintaan miljardi.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=43
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task43 extends TaskBase
{
    /**
     * Weekdays in array.
     *
     * @var array
     */
    private static $weekDays = [
        'maanantai',
        'tiistai',
        'keskiviikko',
        'torstai',
        'perjantai',
        'lauantai',
        'sunnuntai'
    ];

    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $dayString = $this->request->getParam('paiva', '');
        $number    = (int)$this->request->getParam('n', 0);

        $dayNumber = array_search($dayString, self::$weekDays, true);
        $index     = ($dayNumber + $number) % count(self::$weekDays);

        return self::$weekDays[$index];
    }
}
