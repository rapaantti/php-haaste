<?php
namespace classes\tasks;

/**
 * Implementation of Task 21
 *
 * Description:
 *
 * Kalle kirjoittaa kokonaisluvut 1:stä lukuun n. Kuinka monta kertaa Kalle kirjoittaa kunkin numeron? Voit olettaa,
 * että n on korkeintaan 1000.
 *
 * Esimerkiksi jos n on 15, Kalle kirjoittaa luvut 1, 2, 3, ..., 15. Kalle kirjoittaa siis kahdeksan kertaa numeron 1,
 * kaksi kertaa numerot 2, 3, 4 ja 5 sekä kerran numerot 0, 6, 7, 8 ja 9.
 *
 * @see http://www.ohjelmointiputka.net/phph/teht.php?id=21
 *
 * @package classes\tasks
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Task21 extends TaskBase
{
    /**
     * Method that executes class task. Method returns result as string.
     *
     * @return string
     */
    public function exec(): string
    {
        $number = (int)$this->request->getParam('n', 0);

        $output = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        for ($i = 1; $i <= $number; $i++) {
            $numbers = str_split((string)$i);

            foreach ($numbers as $num) {
                $output[$num]++;
            }
        }

        return implode(' ', $output);

    }
}
