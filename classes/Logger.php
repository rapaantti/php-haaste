<?php
namespace classes;

use classes\interfaces\LoggerInterface;

/**
 * Logger class to help debug variables, messages, etc..
 *
 * @package classes
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
class Logger implements LoggerInterface
{
    /**
     * Method to log messages to wanted output.
     *
     * @param mixed $message
     *
     * @return void
     */
    public static function log($message)
    {
        $output = (is_array($message) || is_object($message)) ? print_r($message, true) : $message;

        echo '<pre>' . $output . '<pre>';
    }
}
