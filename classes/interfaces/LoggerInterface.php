<?php

namespace classes\interfaces;

/**
 * Interface for logger
 *
 * @package classes\interfaces
 * @author Antti Rapa <antti.rapa@gmail.com>
 * @version 1.0
 */
interface LoggerInterface
{
    /**
     * Method to log messages to wanted output.
     *
     * @param mixed    $message
     *
     * @return void
     */
    public static function log($message);
}
